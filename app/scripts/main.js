$( document ).ready(function() {

  var header = $('.header'),
      langSwitcher = $('.langswitch__switcher'),
      langSwitcherContainer = $('.langswitch__navigation'),
      langSwitcherNav = $('.langswitch__subnav'),
      footer = $('.footer');

  $(document).foundation();

  $('.js-selectbox').multipleSelect({
    selectAll: false,
    allSelected: false,
    minimumCountSelected: 20,
    width: '100%',
    placeholder: $(this).data("placeholder")
  });

  /* Video Intro */
  var vid = document.getElementById("bgvid");
  var skip = document.getElementsByClassName('intro--skip--button');

  function pauseVid() {
    vid.pause();
  }
  $('#skipIntro').click(function () {
    $('.intro__video__container').fadeOut(500, function () {
      this.remove();
    });
    $('.intro__video__after').fadeIn(500);
    $('.footer').fadeIn(500);
    portfolioScale();
    runSlick();

  });

  $('#bgvid').on('ended', function () {
    $('.intro__video__container').fadeOut(500, function () {
      this.remove();
    });
    $('.intro__video__after').fadeIn(500);
    $('.footer').fadeIn(500)
    portfolioScale();
    runSlick();

  });

  /* Brands Slider settings */
  brandSlickVar = {
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    nextArrow: '<span class="slider-next"></span>',
    prevArrow: '<span class="slider-prev"></span>',
    responsive: [
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  },
  function brandslider() {
    $('.brands__slider').slick({

    });
  }
  runSlick = function() {
    $('.brands__slider').slick(brandSlickVar);
  }

  var $slickElement = $('.event__slider');
  $status = $('.slide__count');
  $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    var i = (currentSlide ? currentSlide : 0) + 1;

    $status.text(i + ' / ' + slick.slideCount);

  });


  $slickElement.slick({
    infinite: true,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    arrows: false,
    focusOnSelect: true,
    variableWidth: true,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          arrows: true,
          nextArrow: '<span class="slick__arrow--bottom page__arrow page__arrow--right"></span>',
          prevArrow: '<span class="slick__arrow--bottom page__arrow page__arrow--left"></span>',
        }
      }
    ]
  });

  $('.read-more-content').addClass('hide')
    .before('<a class="read-more-show" href="#">reed more</a>')
    .append(' <a class="read-more-hide" href="#">roll up</a>');



  $('.read-more-show').on('click', function (e) {
    $(this).next('.read-more-content').removeClass('hide');
    $(this).addClass('hide');
    e.preventDefault();
  });

  $('.read-more-hide').on('click', function (e) {
    $(this).parent('.read-more-content').addClass('hide').parent().children('.read-more-show').removeClass('hide');
    e.preventDefault();
  });
  $('.what__item').on('click', function(e){
    $(this).find('.read-more-show').next('.read-more-content').toggleClass('hide');
    $(this).find('.read-more-show').toggleClass('hide');

  }).on('click', '.read-more-show', function(e){
    e.stopPropagation();
  }).on('click', '.read-more-hide', function(e){
    e.stopPropagation();
  });

  $('.why__slider__container').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    cssEase: 'linear',
    arrows: false,
    dots: false,
    touchMove: false,
    draggable: false,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          dots: true,
          touchMove: true,
          draggable: true
        }
      }
    ]
  });
  $('.why__slide__nav__item').each(function(i) {
    $('.why__slide__nav__item-' + i + ' a').click(function(){
      $('.why__slider__container').slick('slickGoTo', i);
      $('.why__slide__nav__item a').removeClass('active');
      $(this).addClass('active');
    });
  });
  $(window).load(function() {

    $('.why__slider__container .why__slide__container').height($(window).height());
    $('.video-inside').height($(window).height());
  });

  $(window).resize(function() {
    $('.why__slider__container .why__slide__container').height($(window).height());
    $('.video-inside').height($(window).height());
  });

  var $video  = $('video'),
      $window = $(window),
      $videoPlay = $('.video__play__button'),
      $videoClose = $('.video__close__button'),
      $videoContainer = $('.video__container');

  $videoPlay.click(function(){
    $videoContainer.fadeIn(500).css("display","flex");
    $video.play();
  });
  $videoClose.click(function(){
    $videoContainer.fadeOut(500);
    $video.pause();
  });
  $(window).resize(function(){
    var height = $window.height();
    $video.css('height', height);

    var videoWidth = $video.width(),
        windowWidth = $window.width();

    $video.css({
      'height': height
    });
  }).resize();
  function langswitcher() {
    var scrollTop = $(window).scrollTop();
    langSwitcher.on('click', function(){
      $(langSwitcherContainer).toggleClass('active');
      langSwitcherNav.css('top', header.outerHeight());

    });
    $(document).scroll(function(){
      $(langSwitcherContainer).removeClass('active');
    });

  };
  langswitcher();
  function whoAreSlider() {
    var whoOverlay = $('.who__overlay'),
      whoOverlayL = $('.who__overlay--left'),
      whoOverlayR = $('.who__overlay--right'),
      whoPortrait = $('.creator__photo__container'),
      whoPortraitContainerL = $('.creator__portrait-left'),
      whoPortraitContainerR = $('.creator__portrait-right'),
      whoPortraitT = whoPortrait.offset(),
      whoPortraitH = whoPortrait.height(),
      whoSliderClass = $('.who__are__slider');

    whoOverlayL.add(whoPortraitContainerL).click(function () {
      whoSliderClass.slick('slickPrev');
    });

    whoOverlayR.add(whoPortraitContainerR).click(function () {
      whoSliderClass.slick('slickNext');
    });

    $('.who__are__arrow--right').click(function () {
      whoSliderClass.slick('slickNext');
    });

    $('.who__are__arrow--left').click(function () {
      whoSliderClass.slick('slickPrev');
    });
    whoOverlay.mouseleave(function () {
      whoOverlay.removeClass('hover');
    });

    whoSliderClass.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      initialSlide: 1,
      infinite: false,
      arrows: false,
      touchMove: false,
      adaptiveHeight: true
    });
    whoSliderClass.on('afterChange', function (event, slick, currentSlide, nextSlide) {
      $('.who__overlay').removeClass('hover');
    });
  }
  whoAreSlider();
  function portfolioScale() {
    var portfolioContainer    = $('.portfolio'),
        portfolioTile         = $('.portfolio__card__medium'),
        portfolioWrapper      = $('.portfolio__card__wrapper'),
        portfolioTileBig      = $('.portfolio__card__big'),
        portfolioImg          = $('.portfolio__img');

    $(portfolioTile).each(function () {
      portfolioWrapper.height( portfolioTile.width() / 1.20689);
    });
    $(portfolioTileBig).each(function () {
      portfolioWrapper.height( portfolioTileBig.width() / 1.862);
    });

      $(portfolioWrapper).hover( function() {
        var portfolioAnnotation   = $('.card__annotation');
        var portfolioBadge        = $(this).find('.awards__badge__container');
        var portfolioTranslate    = portfolioAnnotation.outerHeight() + portfolioBadge.outerHeight() + 40;
        if (portfolioBadge.outerHeight() !== null) {
          portfolioTranslate = portfolioTranslate + 30;
        }

        $(this).find(portfolioImg).css({
          '-webkit-transform': 'translateY(-'+portfolioTranslate+'px)',
          'transform': 'translateY(-'+portfolioTranslate+'px)'
        });
        console.log(portfolioBadge.outerHeight(),  portfolioBadge.height());
      },
       function() {
         $(this).find(portfolioImg).css({
           '-webkit-transform' : 'translateY(0)',
           'transform': 'translateY(0)'
         });
       });



  }
  portfolioScale();
  $(window).resize(portfolioScale);
  $('.title').gradientText({
    colors: ['#fff', '#ffa9a9'],
    toProcess: ['span']
  });
});
